# Dr0p Bootstrap modifications

Modification made to bootstrap (using scss) to give Dr0p project's styling.

In order to use it:

```html
<html>
  <head>
    ...
    <link rel="stylesheet" href="https://mu-sse.gitlab.io/dr0p/dr0p-bootstrap/css/dr0p_bs.css"/>
    ...
  </head>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
```

## How to compile scss files automatically

Live Sass Compiler -> Glenn Marks

Search for "Watch Sass" on the blue bar of VSCode.